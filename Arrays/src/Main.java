import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        int o = 0;
        float [] a;
        a = new float[6];
        while (o < 6) {
            System.out.print((o+1) + " элемент:");
            Scanner sc = new Scanner(System.in);
            a[o] = sc.nextFloat();
            if (a[o]<0){
                a[o] += -a[o] * 0.1;
            } else {
                a[o] *= 1.1;}
                o++;

        }
        for (int i = a.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (a[j] < a[j + 1]) {
                    float tmp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = tmp;
                }
            }
        }

        o = 0;
        while (o < 6) {
            System.out.println("                    " + (o + 1) + " элемент:" + a[o]);
            o++;
        }
    }
}
